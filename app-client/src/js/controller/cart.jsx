var Item = React.createClass({
	getInitialState: function() {
		var {price,pcs,checked,name} = this.props
		return {
			price,pcs,checked,name
		}
	},
	componentDidMount: function() {
		var checked = this.state.checked
		if(checked) {
			this.props.onChange(this.state.name,this.state.price * this.refs.pcs.getDOMNode().value)
		}else{
			this.props.onChange(0)
		}
	},
	toggle() {
		var checked = !this.state.checked
		this.setState({
			checked
		})
		if(checked) {
			this.props.onChange(this.state.name,(+this.state.price) * +this.refs.pcs.getDOMNode().value)
		}else{
			this.props.onChange(this.state.name,0)
		}
	},
	componentWillReceiveProps: function(nextProps) {
		if(nextProps.checked !== this.props.checked){
			if(nextProps.checked) {
				this.props.onChange(this.state.name,(+this.state.price) * +this.refs.pcs.getDOMNode().value)
			}else{
				this.props.onChange(this.state.name,0)
			}
			this.setState({
				checked: nextProps.checked
			})
		}
	},
	calc(e) {
		var v = e.target.value
		this.state.checked && this.props.onChange(this.state.name,+this.state.price * +v)
	},
	render() {
		console.log('render')
		var {price,checked,pcs} = this.state
		return (
			<div>
				{checked ? <input type="checkbox" checked onChange={this.toggle}/>
					: <input type="checkbox" onChange={this.toggle}/>
				}
				<p>{price}</p>
				<input type="text" defaultValue={pcs} onChange={this.calc} ref="pcs"/>
			</div>
		)
	}
})

var Items = React.createClass({
	getInitialState: function() {
		return {
			all: 0,
			items: {},
			checked: [true,true],
			checkedAll: true
		}
	},
	componentDidMount: function() {
		var items = this.state.items,
			all = 0
		for(var i in items){
			all += items[i]
		}
		this.setState({
			all
		})
	},
	change(k,v) {
		var items = this.state.items,
			all = 0
		items[k] = v
		for(var i in items){
			all += items[i]
		}
		this.setState({
			items,all
		})
	},
	render() {
		var all = this.state.all,
			checked = this.state.checked,
			checkedAll = this.state.checkedAll
		return (
			<div>
				<input type="checkbox" checked={checkedAll} onChange={function(){
					var ck = this.state.checked
					var ret = ck.map(function(c){
						return !c
					})
					this.setState({
						checked: ret,
						checkedAll: !this.state.checkedAll
					})
				}.bind(this)}/>
				<Item price="100" pcs="12" onChange={this.change} checked={checked[0]} name="a1"/>
				<Item price="50" pcs="122" onChange={this.change} checked={checked[1]} name="a2"/>
				<p>{this.state.all}</p>
			</div>
		)
	}
})
var cart = React.createClass({

	render: function() {
		return (
			<div>
				
				<Items />
			</div>
		)
	}

})

module.exports = cart