var Header = require('./base/header'),
	{SearchText} = require('./base/parts'),
	Link = ReactRouter.Link

var {Actions,Store} = require('../store/mainStore')

var Option = React.createClass({
	render() {
		var {title,className,bid} = this.props
		return (
			<dd className={className}>
				<Link to="searchResult" query={{k: title,bid: bid}}>
					{title}
				</Link>
			</dd>
		)
	}
})

var Nav = React.createClass({
	render() {
		var {handler,title,className,key} = this.props
		return (
			<a href={'#'+title}
				onClick={handler}
				className={className}
				key={key}
			>
				{title}
			</a>
		)
	}
})

var brand = React.createClass({
	mixins: [Reflux.listenTo(Store,'getData')],
	getInitialState() {
		return {
			data: null,
			value: ''
		}
	},
	componentDidMount() {
		require('../../css/brand.scss')
		Actions.reset()
		Actions.getData()
	},
	getData(data) {
		this.data = data.api.updates.brands
		this.setState({
			data: data.api.updates.brands
		})
	},
	jumpTo(e) {
		e.preventDefault()
		var target = e.target
		document.getElementById(e.target.innerHTML).scrollIntoView()
	},
	filter(value) {
		var self = this
		if(value){
			this.setState({
				value,
				data: self.data.filter(function(d){
					return d.name.toLowerCase().indexOf(value) !== -1
				})
			})
		}else{
			this.setState({
				data: self.data,
				value
			})
		}
	},
	renderContent() {
		var {data,value} = this.state
		if(data){
			var nav = [],ret = [],curGroupId = '',index = 0
			data.forEach(function(brand){
				if(curGroupId !== brand.group){
					curGroupId = brand.group
					nav.push(<Nav className="jump" title={brand.group} handler={this.jumpTo} key={index}/>)
					ret.push(<dt id={brand.group} key={index}>{brand.group}</dt>)
					index += 1
				}
				ret.push(<Option className="opt" title={brand.name} key={index} bid={brand.id}/>)
				index += 1
			}.bind(this))
			return (
				<section className="main">
					<div className="brandSearch">
						<div className="searchTextWrapper">
							<SearchText value={value} onChange={this.filter}/>
						</div>
					</div>
						<dl className="brandList">
							{ret}
						</dl>
						<div className="brandNav">
							{nav}
						</div>
				</section>
			)
		}else{
			return null
		}
	},
	render() {
		return (
			<div className="app-brand">
				<Header>
					<span>品牌</span>
				</Header>
				{this.renderContent()}
			</div>
		)
	}
})

module.exports = brand
