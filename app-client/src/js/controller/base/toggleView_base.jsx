var M = require('M'),
	Link = ReactRouter.Link
var ToggleViewToggler = React.createClass({
	handler(e) {
		e.preventDefault()
		var {handler} = this.props
		window.callapp && window.callapp.showToast('ssss','snt:app')
		handler()
	},
	componentDidMount() {
		SNT.on('snt:app',function(e,a,b,c){
			console.log(a,b,c)
		})
	},
	componentWillUnmount() {
		SNT.off('snt:app')
	},
	render() {
		var {list} = this.props
		return (
			<a className={list ? 'ico_list' : 'ico_pic'}
				href=""
				onClick={this.handler}
			></a>
		)
	}
})

var ToggleViewContent = React.createClass({
	fixPrice(price) {
		price = parseFloat(price)
		return '￥' + price.toFixed(2)
	},
	renderPrice(price) {
		price = parseFloat(price).toFixed(2)
		price = String(price).split('.')
		var int = price[0]
		return (
			<span className="v">￥<em>{int}</em>.{price[1]}</span>
		)
	},
	calcDiscount(v,lv){
	  return (v / lv * 10).toFixed(1) + '折'
	},
	render() {
		var {list,data,brands} = this.props
		return (
			<ul className={list ? 'list' : 'pic'} key="toggleView">
				{data.map(function(item,index){
					return (
						<li key={index}>
							<Link to="item" params={{itemId: item.id}}>
							<div className="img">
								<img src={item.img} alt="" />
							</div>
							<div className="sum">
								<p>
									<span className="l">{item.l}直邮</span>
								</p>
								<p className="name">
									<span className="b">{item.b}</span><span className="t">{item.t}</span>
								</p>
								<p>
									{this.renderPrice(item.p.v)}
									{item.p.lv && <i className="ico_discount"><em>{this.calcDiscount(item.p.v,item.p.lv)}</em></i>}
								</p>
								{item.p.lv && <p><span className="lv">{this.fixPrice(item.p.lv)}</span></p>}
								{brands[item.bid] && <img src={brands[item.bid]} className="logo"/>}
							</div>
							</Link>
						</li>
					)
				}.bind(this))}
			</ul>
		)
	}
})

var toggleViewMixin = {
	componentWillMount() {
		require('../../../css/toggleView.scss')
	},
	initialize(config,loadingCb) {
		var self = this,
			config = config || {}
		var {container,wrapper,bindScrollEvent} = config
		this.scrollHandler = function(){
			var t = container.scrollTop,
				H = container.scrollHeight,
				h = window.innerHeight
			if(h + t >= H - 300){
				self.loadMore()
			}
		}
		this.setState({
			loading: true,
			loadingCb: loadingCb.bind(self),
			wrapper: M.get(wrapper)
		})
		bindScrollEvent && container.addEventListener('touchend',this.scrollHandler)
	},
	unbind(container) {
		container.removeEventListener('touchend',this.scrollHandler)
	},
	getData(data,maxPage,fresh) {
		// 多次setState问题
		var self = this
		if(fresh){
			this.setState({
				data: data.result,
				maxPage: maxPage,
				page: 0,
				loading: false
			})
		}else{
			this.setState({
				data: this.state.data ? this.state.data.concat(data.result) : data.result,
				maxPage: maxPage,
				page: self.state.page + 1,
				loading: false
			})
		}
	},
	toggle(){
		this.setState({
			list: !this.state.list
		})
	},
	loadMore(){
		var page = this.state.page + 1,
			self = this
		if(page <= this.state.maxPage){
			this.setState({
				loading: true,
				noTransition: true,
				loadingCb: function(){
					self.loadMoreCb(page)
				}
			})
		}
	},
	fresh() {
		var self = this
		this.setState({
			loading: true,
			noTransition: false,
			loadingCb: this.freshCb
		})
	}
}

module.exports = {ToggleViewToggler,ToggleViewContent,toggleViewMixin}
