var {ToggleViewToggler,ToggleViewContent} = require('./toggleView_base')
module.exports = {
	renderView() {
		var {data,brands,list,loading} = this.state
		if(data) {
			return <ToggleViewContent list={list} data={data} brands={brands}/>
		}else{
			if(!loading){
				return <p>暂无内容</p>
			}
		}
	}
}
