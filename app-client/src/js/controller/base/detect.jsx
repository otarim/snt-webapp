// var Action = Reflux.createActions(['detect','destroy'])
// var Store = Reflux.createStore({
// 	listenables: [Action],
// 	onDetect() {
// 		var iframe = document.createElement('iframe'),self = this
// 		iframe.id = "detectFrame"
// 		iframe.setAttribute('width',0)
// 		iframe.setAttribute('height',0)
// 		iframe.style.display = 'none'
// 		document.body.insertBefore(iframe,document.body.firstChild)

// 		this.trigger(true)
// 	},
// 	onDestroy() {
// 		var iframe = document.getElementById('detectFrame')
// 		iframe.parentNode.removeChild(iframe)
// 	}
// })
// var detect = React.createClass({
// 	mixins: [Reflux.listenTo(Store,'getResult')],
// 	statics: {
// 		init: false,
// 		installed: false
// 	},
// 	getResult(installed) {
// 		detect.installed = installed
// 		detect.init = true
// 		this.forceUpdate()
// 	},
// 	componentDidMount: function() {
// 		if(!detect.init){
// 			Action.detect()
// 		}
// 	},
// 	componentWillUnmount: function() {
// 	  Action.destroy()
// 	},
// 	render: function() {
// 		if(detect.init && !detect.installed){
// 			return <div>未安装</div>
// 		}
// 		return null
// 	}
// })

// module.exports = detect
var detect = React.createClass({
	jump(e) {
		e.preventDefault()
		var iframe = this.refs.iframe.getDOMNode()
		iframe.src = 'shangnatao://'
		setTimeout(function(){
			location.href = 'http://www.baidu.com'
		},1)
	},
	componentWillUnmount() {
		// getElement is reject ?
		var iframe = this.refs.iframe.getDOMNode()
		iframe && iframe.parentNode.removeChild(iframe)
	},
	render() {
		return (
			<div>
				<iframe style={{
					display: 'none'
				}} width="0" height="0" ref="iframe"/>
				<a href="" onClick={this.jump}>测试url</a>
			</div>
		)
	}
})

module.exports = detect