var M = require('M')
var TOP = React.createClass({
	getDefaultProps: function() {
		return {
			scrollElem: window
		}
	},
	componentDidMount() {
		var {toTopDistance,toTopCallback,toTopFallback,scrollElem} = this.props
		var el = this.refs.el.getDOMNode()
		this.toTopHandler = function(){
			var distance = parseInt(toTopDistance,10),
				t = M.get(scrollElem).scrollTop(),
				cb = toTopCallback,
				fb = toTopFallback
			if(t > distance){
				el.style.display = 'block'
				cb && cb(toTopEl)
			}else{
				el.style.display = 'none'
				fb && fb(toTopEl)
			}
		}.bind(this)
		scrollElem.addEventListener('scroll',this.toTopHandler)
	},
	componentWillUnmount() {
		var {scrollElem} = this.props
		scrollElem.removeEventListener('scroll',this.toTopHandler)
	},
	toTop(e) {
		e.preventDefault()
		M.get(this.props.scrollElem).scrollTop(0)
	},
	render() {
		return (
			<div className="toTop" ref="el">
				<a href=""
					onClick={this.toTop}
				>返回顶部</a>
			</div>
		)
	}
})


var tools = React.createClass({
	render: function() {
		var useToTop = 'toTop' in this.props
		return (
			<div className="globalTool" ref="toTop">
				{useToTop && <TOP {...this.props}/>}
			</div>
		)
	}
})

module.exports = tools
