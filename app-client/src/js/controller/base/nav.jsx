var	Link = ReactRouter.Link

var nav = React.createClass({
	componentDidMount: function() {
		require('../../../css/nav.scss')
	},
	render: function() {
		return (
			<div className="navbar">
				<ul className="nav">
					<li><Link to="main" activeClassName="on">首页</Link></li>
					<li><Link to="brand" activeClassName="on">品牌</Link></li>
					<li><Link to="search" activeClassName="on">搜索</Link></li>
					<li><Link to="cart" activeClassName="on">购物车</Link></li>
					<li><Link to="dashboard" activeClassName="on">我的</Link></li>
				</ul>
			</div>
		);
	}

})

module.exports = nav
