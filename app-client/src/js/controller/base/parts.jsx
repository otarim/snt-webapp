var Link = ReactRouter.Link,
	M = require('M'),
	cx = require('classSet')
// require base.scss
var BackBtn = React.createClass({
	render() {
		var {to,query,params} = this.props
		return (
			<Link to={to} query={query} params={params}>
				<i className="ico_back"></i>
			</Link>
		)
	}
})

var SearchText = React.createClass({
	getInitialState() {
		return {
			value: this.props.value
		}
	},
	componentDidMount() {
		this.props.focus && this.refs.search.getDOMNode().focus()
	},
	componentWillReceiveProps(nextProps) {
		if(nextProps.value !== this.state.value){
			this.setState({
				value: nextProps.value
			})
		}
	},
	change(e) {
		this.setState({value: e.target.value})
		this.props.onChange && this.props.onChange(e.target.value)
	},
	render() {
		var {useLink,onInput} = this.props,
			value = this.state.value
		if(useLink){
			return (
				<Link to="search" query={{search: value}}>
					<input type="search" value={value} className="searchText" onInput={onInput} onChange={this.change} ref="search"/>
				</Link>
			)
		}else{
			return <input type="search" value={value} className="searchText"onInput={onInput} onChange={this.change} ref="search"/>
		}
	}
})

var AppCover = React.createClass({
	statics: {
		init: (function(){
			var hash = location.hash.slice(1)
			return !(hash === '/main' || hash === '/')
		})(),
		coverTimmer: null
	},
	componentDidMount() {
		if(!AppCover.init){
			AppCover.init = true
			AppCover.coverTimmer = setTimeout(function(){
				var cover = this.refs.cover.getDOMNode()
				M.get(cover).animate({
					opacity: 0
				},function(){
					cover.parentNode.removeChild(cover)
				})
			}.bind(this),1500)
		}
	},
	componentWillUnmount() {
		clearTimeout(AppCover.coverTimmer)
	},
	shouldComponentUpdate(nextProps, nextState) {
		return false
	},
	render() {
		if(!AppCover.init){
			return <div className="app-cover" ref="cover" />
		}else{
			return null
		}

	}
})

var Selector = React.createClass({
  getInitialState() {
    var {curClass,cur,multi,toggle} = this.props
    this.beforeChange = this.props.beforeChange || function(){}
    this.onChange = this.props.onChange || function(){}
    return {
      curClass: curClass || 'cur',
      multi: multi || false,
      toggle: typeof(toggle) === 'undefined' ? true : toggle,
      cur: cur
    }
  },
  handler(e) {
    e.preventDefault()
    var el = M.get(e.target),
      index = parseInt(el.data('index'),10)
    var {curClass,cur,multi,toggle} = this.state
    if(this.beforeChange.call(this,el) === false) return
    if(index === cur){
      if(toggle){
        this.setState({
          cur: null
        })
      }
    }else{
      this.setState({
        cur: index
      })
    }
    this.onChange && this.onChange.call(this,el)
    this.props.handler(el,cur === index)
  },
  render() {
    var {curClass,cur} = this.state
    return (
      <div>
        {this.props.children.map(function(children,index){
          var className = cx({
            [curClass]: cur === index
          })
          return React.cloneElement(children,{
            key: index,
            'data-index': index,
            className: className,
            onClick: this.handler
          })
        },this)}
      </div>
    )
  }
})

var Slider = React.createClass({
  getDefaultProps() {
  	return {
			curClass: 'cur',
  		viewNum: 1,
  		loop: true,
  		onBeforeChange: function(){},
  		onChange: function(){},
  		// preventSwipe: false,
  		duration: 300
  	}
  },
  getInitialState() {
    return {
      index: this.props.index || (this.props.children > 1 && this.props.loop ? 1 : 0),
      transition: 'none' // transition: 0 is invalid
    }
  },
  componentWillMount() {
    var nodes = this.props.children.map(function(children){
			return React.cloneElement(children)
		})
    if(this.props.loop && nodes.length > 1){
      var last = React.cloneElement(nodes[nodes.length - 1]),
        first = React.cloneElement(nodes[0])
      nodes.unshift(last)
      nodes.push(first)
    }
    this.nodes = nodes
    this.len = nodes.length
  },
  componentDidMount() {
    if(this.nodes.length > 1){
      var wrap = M.get(this.refs.wrap.getDOMNode())
      wrap.on('swipeLeft',function(){
        this.moveTo(++this.state.index)
      }.bind(this))
      wrap.on('swipeRight',function(){
        this.moveTo(--this.state.index)
      }.bind(this))
      wrap.on('webkitTransitionEnd',function(){
        var atOnce = false,index,
          {onChange} = this.props
        if(this.state.index === 0){
          atOnce = true
          index = this.len - 2
        }else if(this.state.index === this.len - 1){
          atOnce = true
          index = 1
        }
        if(atOnce) {
          this.setState({
            transition: 'none',
            index: index
          })
        }
        onChange && onChange.call(this)
      }.bind(this))
    }
  },
  moveTo(index) {
    var {duration,onBeforeChange} = this.props
    onBeforeChange && onBeforeChange.call(this)
    this.setState({
      transition: 'all ease '+(duration / 1e3)+'s',
      index: index
    })
  },
  renderNode() {
    var nodes = this.nodes,
      {transition,index} = this.state,
      Tag = this.props.tagName,
      {offset,curClass} = this.props
    return (
      <div className={this.props.className} style={{
					'height': '100%'
				}}>
				<Tag style={{
	        'WebkitTransition': transition,
	        'transition': transition,
	        'WebkitTransform': 'translate3d(-'+(index * offset)+'px,0,0)',
	        'transform': 'translate3d(-'+(index * offset)+'px,0,0)'
	      }} ref="wrap">
	        {nodes.map(function(node,index){
	          return React.cloneElement(node,{
	            key: index
	          })
	        })}
	      </Tag>
				{nodes.length > 1 && <div className="tocs">
					<span className="toc">
						{nodes.slice(2).map(function(node,tocIndex){
							return <a href="" className={index === tocIndex + 1 ? curClass : ''} key={tocIndex}>{tocIndex}</a>
						})}
					</span>
				</div>}
      </div>
    )
  },
  render() {
    return this.renderNode()
  }
})

var Range = React.createClass({
  getDefaultProps: function() {
    return {
      disabledClass: 'disabled',
      onChange: function(){},
      onPeak: function(){},
      onBottom: function(){},
      onIncr: function(){},
      onDecr:function(){},
      step: 1
    }
  },
  getInitialState: function() {
    return {
      value: this.props.value,
      disabled: this.props.disabled,
      disabledIncr: false,
      disabledDecr: false
    }
  },
  componentDidMount: function() {
    this.changeValue(this.state.value)
  },
  changeValue(value) {
    var {min: MIN,max: MAX,onPeak,onBottom} = this.props
    switch(true){
      case value === MIN:
        this.setState({
          value: value,
          disabledDecr: true
        })
        break
      case value === MAX:
        this.setState({
          value: value,
          disabledIncr: true
        })
        break
      case value > MAX:
        this.setState({
          disabledIncr: true,
          value: MAX
        })
        onPeak()
        break
      case value < MIN:
        this.setState({
          disabledDecr: true,
          value: MIN
        })
        onBottom()
        break
      default:
        this.setState({
          value: value,
          disabledDecr: false,
          disabledIncr: false
        })
      }
  },
  render() {
    return (
      <div className={this.props.className}>
        {this.props.children.map(function(children,index){
          var props = Object.assign({},this.props,{
            value: this.state.value,
            changeValue: this.changeValue,
            disabled: this.state.disabled,
            disabledIncr: this.state.disabledIncr,
            disabledDecr: this.state.disabledDecr,
            key: index
          })
          delete props.children
          delete props.className
          return React.cloneElement(children,props)
        },this)}
      </div>
    )
  }
})

var RangeIncr = React.createClass({
  getDefaultProps: function() {
    return {
      className: ''
    }
  },
  handler(e) {
    e.preventDefault()
    if(this.props.disabledIncr) return
    var value = this.props.value + this.props.step
    this.props.changeValue(value)
    this.props.onIncr(value)
  },
  render() {
    var classname = cx({
      [this.props.className]: true,
      [this.props.disabledClass]: this.props.disabledIncr
    })
    return React.cloneElement(this.props.children,{
      onClick: this.handler,
      className: classname
    })
  }
})

var RangeDecr = React.createClass({
  getDefaultProps: function() {
    return {
      className: ''
    }
  },
  handler(e) {
    e.preventDefault()
    if(this.props.disabledDecr) return
    var value = this.props.value - this.props.step
    this.props.changeValue(value)
    this.props.onDecr(value)
  },
  render() {
    var classname = cx({
      [this.props.className]: true,
      [this.props.disabledClass]: this.props.disabledDecr
    })
    return React.cloneElement(this.props.children,{
      onClick: this.handler,
      className: classname
    })
  }
})

var RangeInput = React.createClass({
  getDefaultProps: function() {
    return {
      className: ''
    }
  },
  handler(e) {
    var value = e.target.value
    this.props.changeValue(value)
    this.props.onChange(value)
  },
  change(e) {
    this.setState({value: e.target.value})
  },
  render() {
    var classname = cx({
      [this.props.className]: true,
      [this.props.disabledClass]: this.props.disabledIncr
    })
    return React.cloneElement(this.props.children,{
      value: this.props.value,
      onInput: this.handler,
      onChange: this.change,
      className: classname,
      disable: this.props.disabled
    })
  }
})

var Mask = React.createClass({
  getDefaultProps: function() {
    return {
      opacity: 0.7,
      level: 100
    }
  },
  getInitialState: function() {
    return {
      opacity: this.props.opacity
    }
  },
  componentWillReceiveProps: function(nextProps) {
    var {opacity} = this.props
    if(nextProps.show === true){
      window.requestAnimationFrame(function(){
        this.setState({
          opacity
        })
      }.bind(this),100)
      this.setState({
        show: true
      })
    }else{
      window.requestAnimationFrame(function(){
        this.setState({
          show: false
        })
      }.bind(this),100)
      this.setState({
        opacity: 0
      })
    }
  },
  render(){
    var {opacity,show} = this.state
    return (
      <div style={{
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        background: 'rgba(0,0,0,'+opacity+')',
        zIndex: this.props.level,
        display: show ? 'block' : 'none',
        WebkitTransition: 'all ease .5s',
        transition: 'all ease .5s',
        cursor: 'pointer',
      }}
      {...this.props}
      ></div>
    )
  }
})

var Bounces = React.createClass({
  getDefaultProps: function() {
    return {
     up: true,
     down: true,
     offset: 0,
     threshold: 0,
     onPeak: function() {},
     onBottom: function() {},
     onTouch: function() {}
    }
  },
  getInitialState: function() {
    var offset =  this.props.up ? this.props.offset : 0
    return {
      transition: 'none',
      transform: 'translate3d(0,-'+offset+'px,0)',
      offset
    }
  },
  componentDidMount: function() {
    var p = this.props,
      {offset} = this.state,
      wrap = M.get(this.refs.wrap.getDOMNode()),
      touched = false,
      cbToken = false,
      type,posStart,s
    wrap.on('touchstart',function(e){
      posStart = e.targetTouches[0].pageY
      s = wrap[0].scrollHeight
    }).on('touchmove',function(e){
      var posOffset
      if(p.up){
        if(wrap.scrollTop() === 0){
          if(e.targetTouches[0].pageY < posStart) return
          type = 'up'
          posOffset = -offset + Math.abs(e.targetTouches[0].pageY - posStart) / 4
        } 
      }
      if(p.down){
        if(wrap.scrollTop() + wrap[0].offsetHeight === s){
          if(e.targetTouches[0].pageY > posStart) return
          type = 'down'
          posOffset = -offset - Math.abs(e.targetTouches[0].pageY - posStart) / 4
        } 
      }
      if(type){
        if(Math.abs(posOffset) >= parseInt(p.threshold,10)){
          cbToken = true
        }else{
          cbToken = false
        }
        e.preventDefault()
        p.onTouch(posOffset)
        this.setState({
          transition: 'none',
          transform: 'translate3d(0,'+ posOffset +'px,0)'
        })
      }
    }.bind(this)).on('touchend',function(e){
      if(!type) return
      var {onPeak,onBottom,up,down} = this.props
      if(cbToken){
        cbToken = false
        type === 'up' && onPeak()
        type === 'down' && onBottom()
      }
      type = null
      posStart = 0
      return this.setState({
        transition: 'all ease .5s',
        transform: 'translate3d(0,-'+offset+'px,0)',
      })
    }.bind(this))
  },
  componentWillUnmount: function() {
    M.get(this.refs.wrap.getDOMNode()).off('touchstart').off('touchmove')
  },
  render() {
    var {transform,transition} = this.state
    return (
      <div {...this.props} ref="wrap">
        {React.cloneElement(this.props.children,{
          style: {
            'transition': transition,
            'WebkitTransition': transition,
            'WebkitTransform': transform,
            'transform': transform
          }
        })}
      </div>
    )
  }
})

var Tab = React.createClass({
  getDefaultProps() {
    return {
      // animate: false,
      onChange: function(){},
      onBeforeChange: function(){},
      curIndex: 1,
      curClass: 'cur',
      tag: 'div'
    }
  },
  getInitialState() {
    return {
      curIndex: this.props.curIndex 
    }
  },
  handler(index) {
    if(this.props.onBeforeChange(this.state.curIndex,index) === false) return
    this.setState({
      curIndex: index
    })
    this.props.onChange(index)
  },
  // shouldComponentUpdate(nextProps, nextState) {
  //   return nextState.curIndex !== this.state.curIndex
  // },
  render() {
    var Tag = this.props.tag,
      {animate,onChange,onBeforeChange,curClass} = this.props,
      {curIndex} = this.state
    return ( 
      <Tag {...this.props}>
        {this.props.children.map(function(children,index){
          return React.cloneElement(children,{
            key: index,
            handler: this.handler,
            animate,onChange,onBeforeChange,curIndex,curClass
          })
        },this)}
      </Tag>
    )
  }
})

var TabPanel = React.createClass({
  getDefaultProps() {
    return {
      tag: 'div',
      type: 'Click'
    }
  },
  handler(index,e) {
    e.preventDefault()
    this.props.handler(index + 1)
  },
  render() {
    var Tag = this.props.tag,
      {curIndex,curClass,type} = this.props
    return (
      <Tag {...this.props}>
        {this.props.children.map(function(children,index){
          var classname = cx({
            [children.props.className]: children.props.className,
            [curClass]: index === curIndex - 1
          })
          return React.cloneElement(children,{
            className: classname,
            key: index,
            ['on'+ type]: this.handler.bind(this,index)
          })
        },this)}
      </Tag>
    )
  }
})

var TabList = React.createClass({
  getDefaultProps() {
    return {
      tag: 'div'
    }
  },
  getInitialState() {
    return {
      step: 100 / this.props.children.length 
    }
  },
  calcStep(index) {
    return this.state.step * index
  },
  render() {
    var Tag = this.props.tag,
      {curIndex,curIndex} = this.props,
      {step} = this.state
    return (
      <Tag {...this.props} 
        style={{
          WebkitTransition: 'all ease-in-out .2s',
          transition: 'all ease-in-out .2s',
          WebkitTransform: 'translate3d(-'+this.calcStep(curIndex - 1)+'%,0,0)',
          transform: 'translate3d(-'+this.calcStep(curIndex - 1)+'%,0,0)',
          width: 100 * this.props.children.length + '%'
        }}
      >
        {this.props.children.map(function(children,index){
          return React.cloneElement(children,{
            style: {
              // display: index === curIndex - 1 ? 'block' : 'none'
              display: 'inline-block',
              width: step + '%'
            },
            key: index
          })
        },this)}
      </Tag>
    )
  }
})


var Lazyload = React.createClass({
  statics: {
    binded: false,
    queue: {},
    uuid: 1
  },
  getDefaultProps: function() {
    return {
      el: window,
      threshold: 0
    }
  },
  handler() {
    Object.keys(Lazyload.queue).forEach(function(id){
      var queue = Lazyload.queue[id]
      for (var i = 0; i < queue.length; i++) {
        var img = queue[i];
        if (this.calcPos(img)) {
          this.loadTrue(img)
          queue.indexOf(img) !== -1 && queue.splice(queue.indexOf(img), 1)
          i--
        }
      }
      if(!queue.length) {
        Lazyload.queue[id] = null
        delete Lazyload.queue[id]
      }
    },this)
    if(!Object.keys(Lazyload.queue).length){
      M.get(window).off('touchend',this.handler)
    }
  },
  bindEvent() {
    M.get(window).on('touchend',this.handler)
  },
  calcPos(img) {
      var imgPos_t = img.getBoundingClientRect().top - M.get(this.props.el).offset().top,
      winT = M.get(this.props.el).scrollTop() + parseInt(this.props.threshold,10),
      winH = M.get(this.props.el).height(),
      imgPos_b = winT + imgPos_t + img.height
      console.log(winT,winH)
      return (imgPos_t > 0 && imgPos_t < winH || imgPos_b > winT && imgPos_b < winT + winH)
  },
  loadTrue(img) {
    img.src = M.get(img).data('lazy')
  },
  componentDidMount() {
    this.qname = 'q_' + Lazyload.uuid
    Lazyload.uuid += 1
    if(!Lazyload.binded){
      Lazyload.binded = true
      // bindEvent
      this.bindEvent()
    }
    // add queue
    Lazyload.queue[this.qname] = []
    ;[].forEach.call(this.getDOMNode().querySelectorAll('[data-lazy]'),function(img){
      Lazyload.queue[this.qname].push(img)
    },this)
  },
  componentWillUnmount() {
    Lazyload.binded = false
    Lazyload.queue[this.qname] = null
    delete Lazyload.queue[this.qname]
    if(!Object.keys(Lazyload.queue).length){
      M.get(window).off('touchend',this.handler)
    }
  },
  render() {
    return this.props.children
  }
})

module.exports = {
	BackBtn,SearchText,AppCover,Selector,Slider,Range,RangeIncr,RangeDecr,RangeInput,Mask,Bounces,Tab,TabPanel,TabList,Lazyload
}
