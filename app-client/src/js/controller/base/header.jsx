var header = React.createClass({
	componentDidMount() {
		require('../../../css/header.scss')
	},
	render() {
		return (
			<header className="header">
				{this.props.children}
			</header>
		)
	}
})

module.exports = header
