var $ = require('M').get

var {Actions,Store} = require('../../store/recommend')

var refresher = React.createClass({
	mixins: [Reflux.listenTo(Store,'getData')],
	getData() {
		this.fetching = false
	},
	componentWillReceiveProps(nextProps) {
		if(nextProps.loading === this.props.loading) return
		clearTimeout(this.timmer)
		var h = $(this.refs.refresher.getDOMNode()).height(),
			refresher_wrapper = this.refs.refresher_wrapper.getDOMNode()
		if(nextProps.wrapper && !this.wrapper){
			this.wrapper = nextProps.wrapper
			this.bindEvent()
		}
		if(this.wrapper){
			if(nextProps.loading){
				if(nextProps.noTransition) return nextProps.loadingCb()
				$(refresher_wrapper).addClass('ready')
				this.wrapper.animate({
					'-webkit-transform': 'translate3d(0,0,0)',
					'transform': 'translate3d(0,0,0)'
				})
				nextProps.loadingCb()
			}
			if(!nextProps.loading){
				if(!nextProps.noTransition){
					this.timmer = setTimeout(function(){
							this.wrapper.animate({
							'-webkit-transform': 'translate3d(0,-'+h+'px,0)',
							'transform': 'translate3d(0,-'+h+'px,0)'
						},function(){
							$(refresher_wrapper).removeClass('ready')
						})
					}.bind(this),500)
				}
			}
		}
	},
	componentWillUnmount() {
		clearTimeout(this.timmer)
		this.wrapper.parent().off('touchstart').off('touchmove')
	},
	bindEvent() {
		var posStart = 0,h = $(this.refs.refresher.getDOMNode()).height(),touched = false,self = this,
			refresher_ico = this.refs.refresher_ico.getDOMNode(),
			refresher_inner = this.refs.refresher_inner.getDOMNode(),
			refresher_wrapper = this.refs.refresher_wrapper.getDOMNode(),
			h_ico = $(refresher_ico).height(),
			wrapper = this.wrapper,
			cbToken = false,
			finger = null 
		// wrapper.css({
		// 	'-webkit-transform': 'translate3d(0,-'+h+'px,0)',
		// 	'transform': 'translate3d(0,-'+h+'px,0)'
		// })
		this.fetching = false
		wrapper.parent().on('touchstart',function(e){
			posStart = e.targetTouches[0].pageY
		}).on('touchmove',function(e){
			if($(this).scrollTop() === 0){
				if(e.targetTouches[0].pageY < posStart) return
				e.preventDefault()
				touched = true
				self.fetching = true
					wrapper.css({
					transition: ''
				})
				var posOffset = -h + Math.abs(e.targetTouches[0].pageY - posStart) / 2
				var icoHeight =  h_ico - posOffset - 30
				if(icoHeight < 0){
					icoHeight = 0
					cbToken = true
				}else{
					cbToken = false
				}
				$(refresher_inner).css({
					top: icoHeight + 'px'
				})
				wrapper.css({
					'-webkit-transform': 'translate3d(0,'+ posOffset +'px,0)',
					'transform': 'translate3d(0,'+ posOffset +'px,0)'
				})
			}
		}).on('touchend',function(e){
			if(!touched) return
			touched = false
			if(cbToken){
				cbToken = false
				$(refresher_wrapper).addClass('ready')
				self.props.fresh && self.props.fresh()
			}else{
				$(refresher_wrapper).removeClass('ready')
					wrapper.animate({
					'-webkit-transform': 'translate3d(0,-'+h+'px,0)',
					'transform': 'translate3d(0,-'+h+'px,0)'
				})
			}
			posStart = 0
		})
		// Actions.getData(this.props.type,'p=0'+this.props.condition)
	},
	render() {
		return (
			<div className="refresh" ref="refresher">
				<div className="ico_refresh_wrapper" ref="refresher_wrapper">
					<div className="ico_refresh" ref="refresher_ico">
						<div className="ico_refresh_inner" ref="refresher_inner"></div>
					</div>
					<span className="ico_refresh_radius"></span>
				</div>
			</div>
		)
	}
})

module.exports = refresher
