var RouteHandler = ReactRouter.RouteHandler,
	Nav = require('./base/nav'),
	device = require('../util').device
var { CSSTransitionGroup } = React.addons,
	cx = require('classSet')


var app = React.createClass({
	render: function() {
		var className = cx({
			'app': true,
			'app_webView': device.isWebView
		})
		return (
			<div className={className}>
				<CSSTransitionGroup transitionName="example">
					<RouteHandler prePath={this.props.prePath}/>
				</CSSTransitionGroup>
				{!device.isWebView && <Nav />}
			</div>
		)
	}
})

module.exports = app
