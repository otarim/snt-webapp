var Header = require('./base/header'),
	Refresher = require('./base/refresher'),
	AppCover = require('./base/parts').AppCover,
	Detect = require('./base/detect'),
	recommendMixin = require('./base/recommend_base'),
	Link = ReactRouter.Link,
	M = require('M'),
	util = require('../util'),
	device = util.device

import {ToggleViewToggler,ToggleViewContent,toggleViewMixin} from './base/toggleView_base'

var {Actions,Store} = require('../store/mainStore')

var recommendActions = require('../store/recommend').Actions,
	recommendStore = require('../store/recommend').Store

var Sorts = React.createClass({
	componentDidMount() {
		document.title = 'SNT'
		require('../../css/sorts.scss')
	},
	renderSorts() {
		var data = this.props.data.api.updates.categories
		return data.map(function(sort,index){
			return (
				<div className="sorts-item" key={index}
				>
					<Link to="sort" params={{sortType: sort.sort_order}}>
						<div className="sorts-wrapper">
							<img src={sort.logo} />
							<p>{sort.name}</p>
						</div>
					</Link>
				</div>
			)
		})
	},
	render() {
		var {data} = this.props
		if(data){
			return (
				<div className="sorts">
					{this.renderSorts()}
				</div>
			)
		}else{
			return null
		}
	}
})

var Qrcode = React.createClass({
	scan(e) {
		e.preventDefault()
	},
	render() {
		return (
			<a className="ico_qrcode" onClick={this.scan}/>
		)
	}
})

var main = React.createClass({
	mixins: [Reflux.listenTo(Store,'getInitData'),Reflux.listenTo(recommendStore,'getData'),recommendMixin,toggleViewMixin],
	getInitData(data) {
		var self = this
		this.setState({
			initData: data,
			brands: util.getBrands(data)
		})
		recommendActions.fresh(this.state.type,'p=0')
	},
	getInitialState() {
		return {
			data: null,
			initData: null,
			list: true,
			page: 0,
			type: 'global'
		}
	},
	componentDidMount() {
		this.initialize({
			container: this.refs.recommend.getDOMNode(),
			wrapper: this.refs.wrapper.getDOMNode(),
			bindScrollEvent: true
		},function(){
			Actions.getData()
		})
	},
	componentWillUnmount() {
		this.unbind(this.refs.recommend.getDOMNode())
	},
	loadMoreCb(page) {
		return recommendActions.getData(this.state.type,'p='+page)
	},
	freshCb() {
		return recommendActions.fresh(this.state.type,'p=0')
	},
	render() {
		var {data,initData,list,loading,noTransition,loadingCb,wrapper} = this.state
		return (
			<div className="app-main">
				{!device.isWebView && <AppCover />}
				<Header>
					<span className="ico_logo"></span>
					<Qrcode />
				</Header>
				<section className="main" ref="recommend">
					<div className="wrap" ref="wrapper">
						<Refresher
							loading={loading}
							loadingCb={loadingCb}
							fresh={this.fresh}
							wrapper={wrapper}
							noTransition={noTransition}
						/>
					<Sorts data={initData}/>
						<div className="recommend toggleView">
							<header>
								<span className="title">全球精选</span>
								<ToggleViewToggler list={list} handler={this.toggle}/>
							</header>
							{this.renderView()}
						</div>
					</div>
				</section>
			</div>
		)
	}
})

module.exports = main
