var Route = ReactRouter.Route,
	DefaultRoute = ReactRouter.DefaultRoute,
	Redirect = ReactRouter.Redirect,
	App = require('./app'),
	Main = require('./main'),
	Search = require('./search'),
	Sort = require('./sort'),
	Brand = require('./brand'),
	SearchResult = require('./searchResult'),
	Cart = require('./cart'),
	Item = require('./item'),
	Test = require('./test')

module.exports = (
	<Route>
		<Route handler={App} path="/">
			<Route handler={Main} path="main" name="main" />
			<Route handler={Search} path="search" name="search" />
			<Route handler={Brand} path="brand" name="brand" />
			<Route handler={Cart} path="cart" name="cart" />
			<Route path="dashboard" name="dashboard" />
			<DefaultRoute handler={Main} />
		</Route>
		<Route handler={Sort} path="/sort/:sortType" name="sort" />
		<Route handler={SearchResult} path="searchResult" name="searchResult" />
		<Route handler={Item} path="/items/:itemId" name="item" />
		<Route handler={Test} path="/test" name="test" />
	</Route>
)
