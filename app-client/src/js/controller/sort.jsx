var Link = ReactRouter.Link,
	M = require('M'),
	{ToggleViewToggler,ToggleViewContent,toggleViewMixin} = require('./base/toggleView_base'),
	{BackBtn,SearchText} = require('./base/parts'),
	recommendMixin = require('./base/recommend_base'),
	Refresher = require('./base/refresher'),
	util = require('../util')

var {Actions,Store} = require('../store/mainStore')

var recommendActions = require('../store/recommend').Actions,
	recommendStore = require('../store/recommend').Store

var SortType = React.createClass({
	renderContent() {
		var {data} = this.props
		return (
			<div className="sort">
				<ul>
					{data.subs.map(function(item,index){
						var to = "/searchResult?k="+item.name+"&cid="+data.id+"&cid2="+item.id
						return (
							<li key={index}>
								<Link to={to}>
									<img src={item.logo} />
									<p className="title">
										{item.name}
									</p>
								</Link>
							</li>
						)
					})}
				</ul>
			</div>
		)
	},
	render() {
		if(this.props.data){
			return (
				<div className="sortType">
					{this.renderContent()}
				</div>
			)
		}else{
			return null
		}
	}
})

var sort = React.createClass({
	mixins: [Reflux.listenTo(Store,'getSortData'),Reflux.listenTo(recommendStore,'getData'),recommendMixin,toggleViewMixin],
	getInitialState: function() {
		return {
			data: null,
			sortData: null,
			list: true,
			page: 0
		}
	},
	getSortData(data,fresh) {
		var self = this,
			sortData = this.getCategorie(data.api.updates.categories)
		if(!fresh){
			this.refs.sort.getDOMNode().addEventListener('scroll',this.scrollHandler)
		}
		this.setState({
			initData: data,
			sortData: sortData,
			brands: util.getBrands(data)
		})
		recommendActions.fresh(sortData.id,'p=0&cid='+sortData.id,true)
	},
	getCategorie(data) {
		var id = +this.props.params.sortType
		for(let cat of data){
			if(cat.sort_order === id){
				return cat
			}
		}
	}, 
	componentWillMount: function() {
		require('../../css/sortType.scss')
	},
	componentDidMount() {
		this.initialize({
			container: this.refs.sort.getDOMNode(),
			wrapper: this.refs.wrapper.getDOMNode(),
			bindScrollEvent: false
		},function(){
			Actions.reset()
			Actions.getData()
		})
	},
	componentWillUnmount() {
		this.unbind(this.refs.sort.getDOMNode())
	},
	loadMoreCb(page) {
		return recommendActions.getData(this.state.type,'p='+page)
	},
	freshCb() {
		return Actions.fresh()
	},
	render() {
		var {data,list,loading,noTransition,loadingCb,sortData,wrapper} = this.state
		return (
			<div className="app-sort">
				<header>
					<BackBtn to="main" />
					<div className="searchTextWrapper">
						<SearchText value={sortData && sortData.name || ''} useLink />
					</div>
				</header>
				<section className="main" ref="sort">
					<div className="wrap" ref="wrapper">
						<Refresher
							loading={loading}
							loadingCb={loadingCb}
							fresh={this.fresh}
							wrapper={wrapper}
							noTransition={noTransition}
						/>
						<SortType
							data={sortData}
						/>
						<div className="toggleView">
							<header>
								<span className="title">精选</span>
								<ToggleViewToggler list={list} handler={this.toggle}/>
							</header>
							{this.renderView()}
						</div>
					</div>
				</section>
			</div>
		)
	}
})

module.exports = sort
