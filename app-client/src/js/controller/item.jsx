var M = require('M'),
  util = require('../util'),
  cx = require('classSet'),
  {Selector,BackBtn,Slider,Range,RangeDecr,RangeIncr,RangeInput,Mask,Bounces,Tab,TabPanel,TabList,Lazyload} = require('./base/parts')

var itemActions = require('../store/itemStore').Actions,
  itemStore = require('../store/itemStore').Store

var {Actions,Store} = require('../store/mainStore')

var Gallery = React.createClass({
  getInitialState: function() {
    return {
      data: this.props.data
    }
  },
  componentWillMount: function() {
    // util.adpImage 适配高清图片 ！size
    this.imgWidth = M.get(window).width()
  },
  render() {
    var {data} = this.state
    return (
      <div className="gallery">
        <Slider tagName="ul" offset={this.imgWidth} className="slide">
          {data.imgs.map(function(img,index){
            return (
              <li key={index} ref={index} style={{
                  width: this.imgWidth
                }}>
                <img src={img} />
              </li>
            )
          },this)}
        </Slider>
      </div>
    )
  }
})

var ChooseItem = React.createClass({
  getInitialState: function() {
    var {data} = this.props
    this.condition = {}
    return {
      itemImg: data.dimgs[0],
      data,
      value: 1,
      min: 1,
      max: 99
    }
  },
  changeType(el,hasClass) {
    var type = el.data('type'),
      value = el.data('value')
    this.type = type
    this.value = value
    // 选中当前类型，查找存在当前类型的所有可选项
    // 如果已选类型不存在可选类型，删除该类型
    if(hasClass){
      this.condition[type] = null
      delete this.condition[type]
    }else{
      this.condition[type] = value
      this.changeImage()
    }
    this.filterCondition()
  },
  changeImage() {
    var {aimgs} = this.state.data,
      type = this.type.toLowerCase(),
      img
    if(aimgs[type]){
      img = aimgs[type][this.value]
      this.setState({
        itemImg: img
      })
    }
  },
  filterCondition() {
    var {data} = this.state,
      condition = this.condition
    // if(!Object.keys(condition).length){
    //   return data.aids.forEach(function(type){
    //     M.get('[data-type="'+type+'"]').removeClass('xxx')
    //   })
    // }
    // 复位所有状态
    data.aids.forEach(function(type){
      M.get('[data-type="'+type+'"]').removeClass('xxx')
    })
    console.log(this.condition)

    if(!this.condition[this.type]) return

    // var catArr = Object.keys(condition).reduce(function(arr,type){
    //   return arr.concat(data.attrs[type][condition[type]])
    // },[])
    console.log(data.attrs)
    var catArr = data.attrs[this.type][this.condition[this.type]]
    var filter = util.combineObj(util.filterKey(catArr.map(function(itemId){
      // console.log(data.skus[itemId].attr,condition)
      return data.skus[itemId].attr
    }),{
      [this.type]: this.condition[this.type]
    }))
    console.log(filter)
    // 非当前属性的选项设置为可用
    Object.keys(filter).forEach(function(type){
      // console.log(type)
      M.get('[data-type="'+type+'"]').addClass('xxx')
      filter[type].forEach(function(val){
        M.get('[data-value="'+val+'"]').removeClass('xxx')
      })
    })
  },
  render() {
    var {data,itemImg,min,max,value} = this.state,
      {closeEvent} = this.props,
      self = this
    return (
      <div className={this.props.className}>
        <div className="hd">
          <div className="itemImg">
            <img src={itemImg}/>
          </div>
          <div className="sum">
            <h5>{data.t}</h5>
          </div>
          <a className="ico_del" href="" onClick={closeEvent}></a>
        </div>
        <div className="bd">
          <div className="attrs">
            {Object.keys(data.attrs).map(function(key,index){
              return (
                <dl key={index}>
                  <dt>{attrs[key].key.name}</dt>
                  <Selector handler={self.changeType} beforeChange={function(el){
                      if(el.hasClass('xxx')){
                        return false
                      }
                    }}>
                    {attrs[key].values.map(function(value,index){
                      return <dd
                        key={index}
                        data-value={value.id}
                        data-type={key}
                        >{value.name}</dd>
                    },this)}
                  </Selector>
                </dl>
              )
            })}
          </div>
          <Range min={min} max={max} value={value} className="range">
            <RangeDecr className="decr">
              <a href=""></a>
            </RangeDecr>
            <RangeInput className="input">
              <input type="text" name="" id="" />
            </RangeInput>
            <RangeIncr className="incr">
              <a href=""></a>
            </RangeIncr>
          </Range>
        </div> 
        <div className="ft">
          <div className="flex-wrapper">
            <a href="" className="btn_addChat">加入购物车</a>
            <a href="" className="btn_buy">立即购买</a>
          </div> 
        </div>
      </div>
    )
  }
})

// var Promise = React.createClass({
//   render() {
//     return (
//       <div className="promise">
//         <div className="hd">
//           <span>上哪淘保证</span>
//         </div>
//         <ul className="bd flex-wrapper">
//           <li>
//             <img src="" alt="" />
//             <p>正品保证</p>
//           </li>
//           <li>
//             <img src="" alt="" />
//             <p>海外直供</p>
//           </li>
//           <li>
//             <img src="" alt="" />
//             <p>极速发货</p>
//           </li>
//           <li>
//             <img src="" alt="" />
//             <p>无忧退货</p>
//           </li>
//         </ul>
//       </div>
//     )
//   }
// })

var Promise = React.createClass({
  render() {
    return (
      <div className="promise">
        <img src="./dist/img/promise.png"/>
      </div>
    )
  }
})

var Item = React.createClass({
  mixins: [Reflux.listenTo(itemStore,'getData'),Reflux.listenTo(Store,'getInitData')],
  getInitialState: function() {
    return {
      data: null,
      showDetail: false,
      subTitle: '',
      bounceTip: '下拉关闭图文详情'
    }
  },
  getData(data) {
    this.setState({
      data
    })
    var header = M.get(this.refs.header.getDOMNode())
    M.get(this.refs.main.getDOMNode()).on('touchmove',function(e){
      var st = this.scrollTop
      if(st < 50){
        header.css({
          background: 'rgba(246, 242, 242,' + st / 50 + ')'
        })
      }else{
        header.css({
          background: 'rgba(246, 242, 242,1)'
        })
      }
    })
  },
  getInitData(data) {
    var self = this
		this.setState({
			initData: data,
			brands: util.getBrands(data)
		})
    itemActions.getData(this.props.params.itemId)
  },
  fixPrice(price) {
		price = parseFloat(price)
		return '￥' + price.toFixed(2)
	},
	renderPrice(price) {
		price = parseFloat(price).toFixed(2)
		price = String(price).split('.')
		var int = price[0]
		return (
			<span className="v">￥<em>{int}</em>.{price[1]}</span>
		)
	},
  calcDiscount(v,lv){
    return (v / lv * 10).toFixed(1)  + '折'
  },
  componentWillMount: function() {
    require('../../css/item.scss')
  },
  componentDidMount: function() {
    Actions.getData()
  },
  componentWillUnmount: function() {
    M.get(this.refs.main.getDOMNode()).off('touchmove')
  },
  showOptions(e) {
    e.preventDefault()
    this.setState({
      showOptions: !this.state.showOptions
    })
  },
  hideItemDetail() {
    if(!this.state.showDetail) return 
    this.setState({
      showDetail: false,
      subTitle: ''
    })
  },
  showItemDetail() {
    if(this.state.showDetail) return 
    this.setState({
      showDetail: true,
      subTitle: '图文详情'
    })
  },
  render() {
    var {data,brands,showOptions,showDetail,subTitle,bounceTip} = this.state
    if(data) {
      var shockout = cx({
        'tips': true,
        'hadShockOut': !data.on
      })
      var wrapperClass = cx({
        containner: true,
        out: showOptions,
        in: showOptions === false
      })
      var ChooseItemClass = cx({
        chooseItem: true,
        in: showOptions,
        out: showOptions === false
      })
      var DetailClass = cx({
        itemDetail: true,
        in: showDetail,
        out: showDetail === false
      })
      return (
        <div className="snt-item">
          <div className={wrapperClass} ref="animate">
            <header ref="header">
              <BackBtn to={this.props.prePath || 'main'} />
              <h5>{subTitle}</h5>
              <aside className="fns">
              </aside>
            </header>
            <Bounces className="bounce" ref="main" onBottom={this.showItemDetail} threshold="50">
            <section>
              <div className="item">
                <Gallery data={data}/>
                <div className="sum">
                  <p className="l">{data.l}</p>
                  <div className="t">
                    <p>{data.t}</p>
                    <p>{data.b}</p>
                    <a href="" className="share">
                      <i className="ico_share"></i>
                      分享
                    </a>
                  </div>
                  <p>
  									{this.renderPrice(data.p.v)}
                    {data.p.lv && <i className="ico_discount"><em>{this.calcDiscount(data.p.v,data.p.lv)}</em></i>}
  								</p>
  								{data.p.lv && <p><span className="lv">{this.fixPrice(data.p.lv)}</span></p>}
                  {brands[data.bid] && <img src={brands[data.bid]} className="logo"/>}
                </div>
              </div>
              <div className="service">
                <ul>
                  <li className="fn" onClick={this.showOptions}>
                    请选择产品参数
                  </li>
                  <li>配送费：至<em className="l">广州</em></li>
                  <li>关税：本商品适用税率为0%</li>
                </ul>
              </div>
              <Promise />
              <p className="bounceTip">
                上拉查看图文详情
              </p>
            </section>
            </Bounces>
            
            <section className={DetailClass}>
              <Tab>
                <TabPanel className="tabPanel">
                  <a href=""><span>图文详情</span></a>
                  <a href=""><span>规格参数</span></a>
                  <a href=""><span>产品FAQ</span></a>
                </TabPanel>
                <TabList className="tabList">
                  
                    <div className="tab">
                    <Bounces onPeak={this.hideItemDetail} className="bounceTab" threshold="40" offset="21" onTouch={function(n){
                      console.log(n)
                      if(n >= 40){
                        this.setState({
                          bounceTip: '松手关闭图文详情'
                        })
                      }else{
                        this.setState({
                          bounceTip: '下拉关闭图文详情'
                        })
                      }
                    }.bind(this)}>
                    <div>
                    <p className="bounceTip">
                      {bounceTip}
                    </p>
                    <Lazyload el=".bounceTab">
                    <div dangerouslySetInnerHTML={{
                      __html: util.lazy(data.desc)
                    }}></div>
                    </Lazyload>
                    </div>
                    </Bounces>
                    
                    </div>
                  <div className="tab">1</div>
                  <div className="tab">2</div>
                </TabList>
              </Tab>
            </section>
            
            <div className={shockout}>已下架</div>
            <div className="service-hook">
              <div className="flex-wrapper">
                <a href="" className="btn_addChat">加入购物车</a>
                <a href="" className="btn_buy" onClick={this.showOptions}>立即购买</a>
              </div> 
            </div>
          </div>
          <Mask onClick={this.showOptions} show={showOptions}/>
          <ChooseItem data={data} className={ChooseItemClass} closeEvent={this.showOptions}/>
        </div>
      )
    }else{
      return null
    }
  }
})

module.exports = Item
