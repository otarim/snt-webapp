var Header = require('./base/header'),
	{SearchText} = require('./base/parts'),
	Link = ReactRouter.Link,
	M = require('M'),
	config = require('../config.js'),
	util = require('../util.js')

var Actions = Reflux.createActions(['hotTag','suggest'])

var HotTagStore = Reflux.createStore({
	listenables: [Actions],
	onHotTag() {
		var self = this
		if(this.data){
			return this.trigger(this.data)
		}
		M.ajax({
			url: config.api.url + '/search/hot',
			crossdomain: true,
			dataType: 'json',
			success(data){
				self.data = data
				self.trigger(data)
			}
		})
	}
})

var HotTag = React.createClass({
	mixins: [Reflux.listenTo(HotTagStore,'getData')],
	getInitialState() {
		return {
			data: null
		}
	},
	componentDidMount: function() {
		Actions.hotTag()
	},
	getData(data) {
		this.setState({
			data: data
		})
	},
	renderTag() {
		var {data} = this.state
		if(data){
			return data.map(function(tag,index){
				return (
					<Link to="searchResult" query={{k: tag}} key={index} className="tag">
						{tag}
					</Link>
				)
			})
		}else{
			return null
		}
	},
	render() {
		return (
			<div className="searchHotTag">
				<header><span className="ico_hotTag"></span>热门搜索</header>
				<section>
					{this.renderTag()}
				</section>
			</div>
		)
	}
})

var SearchHistory = React.createClass({
	renderContent() {
		var historys = util.searchHistory.getAll()
		if(historys && historys.length){
			return (
				<ul>
					{historys.map(function(value,index){
						return <li key={index}><Link to="searchResult" query={{k: value}}>{value}</Link></li>
					})}
				</ul>
			)
		}else{
			return (
				<p>暂无搜索历史</p>
			)
		}
	},
	render() {
		return (
			<div className="searchHistory">
				<header><span className="ico_history"></span>搜索历史</header>
				<section>
					{this.renderContent()}
				</section>
			</div>
		)
	}
})

var SuggestStore = Reflux.createStore({
	listenables: [Actions],
	onSuggest(val) {
		var self = this
		M.ajax({
			url: config.api.url + '/search/suggest',
			data: {
				k: val
			},
			type: 'get',
			dataType: 'json',
			success(data){
				self.trigger(data)
			}
		})
	}
})

var Suggest = React.createClass({
	mixins: [Reflux.listenTo(SuggestStore,'getData')],
	getInitialState: function() {
		return {
			data: [],
			display: this.props.display || false
		}
	},
	getData(data) {
		this.setState({
			data: data,
			display: !!data.length
		})
		this.props.changeDisplay(!!data.length)
	},
	renderData() {
		var {data} = this.state
		if(data && data.length){
			return (
				<ul>
					{data.map(function(word,index){
						return <li key={index}><Link to="searchResult" query={{k: word}}>{word}</Link></li>
					})}
				</ul>
			)
		}else{
			return null
		}
	},
	componentWillReceiveProps: function(nextProps) {
		if(nextProps.display !== this.state.display){
			this.setState({
				display: nextProps.display
			})
		}
	},
	render() {
		var {display,data} = this.state
		return (
			<div className="searchSuggest"
				style={{
					display: display ? 'block' : 'none'
				}}
			>
				{this.renderData()}
			</div>
		)
	}
})

var search = React.createClass({
	statics: {
		willTransitionFrom(transition, component) {
			var query = location.hash.split('?')[1]
			if(query){
				var vals = query.split('&')
				if(vals.length){
					for(let value of vals){
						// did work in safari,need polyfill
						let tmp = value.split('=')
						if(tmp[0] === 'k' && !tmp[1].trim()){
							transition.abort()
							break
						}
					}
				}
			}
		}
	},
	getInitialState: function() {
		return {
			value: this.props.query.search || '',
			canGoBack: true,
			display: false
		}
	},
	componentDidMount() {
		require('../../css/search.scss')
		document.title = 'search'
	},
	changeDisplay(display) {
		if(display !== this.state.display){
			this.setState({display})
		}
	},
	suggest(e) {
		// ios 第三方键盘不支持keyXXX事件，使用input替代
		var val = e.target.value
		clearTimeout(this.timmer)
		this.timmer = setTimeout(function(){
			Actions.suggest(val)
		},50)
	},
	getVal(val) {
		this.setState({
			canGoBack: false,
			value: val
		})
	},
	clear(e) {
		e.preventDefault()
		this.setState({
			value: '',
			canGoBack: false,
			display: false
		})
	},
	render() {
		var {value,canGoBack,display} = this.state
		return (
			<div className="app-search">
				<Header>
					<div className="searchBar">
						<div className="searchTextWrapper">
							<SearchText value={value} onInput={this.suggest} onChange={this.getVal} focus/>
							{value && <a href="" className="ico_del" onClick={this.clear}></a>}
						</div>
						{canGoBack && <Link to={this.props.prePath || 'main'} className="btn_cancle">
							取消
						</Link>}
						{!canGoBack && <Link to="searchResult" className="btn_search" query={{k: this.state.value}}>
							搜索
						</Link>}
					</div>
				</Header>
				<section className="main searchContainer">
					<HotTag />
					<SearchHistory />
				</section>
				<Suggest
					display={display}
					changeDisplay={this.changeDisplay}
				/>
			</div>
		)
	}
})

module.exports = search
