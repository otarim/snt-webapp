var Refresher = require('./base/refresher'),
	{ToggleViewToggler,ToggleViewContent,toggleViewMixin} = require('./base/toggleView_base'),
	{BackBtn,SearchText} = require('./base/parts'),
	cx = require('classSet'),
	Link = ReactRouter.Link,
	M = require('M'),
	util = require('../util.js')

var {Actions,Store} = require('../store/mainStore')

var searchActions = require('../store/searchStore').Actions,
	searchStore = require('../store/searchStore').Store

var SearchOptions = React.createClass({
	getInitialState() {
		return {
			type: 'default',
			asc: true
		}
	},
	handler(e) {
		e.preventDefault()
		var type = M.get(e.target).parent().data('type'),
			curType = this.state.type,
			{changeSearchType,changePriceType} = this.props
		if(type === curType){
			if(type === 'price'){
				var sort = !this.state.asc
				this.setState({
					asc: sort
				})
				return changePriceType(sort)
			}else{
				return
			}
		}
		this.setState({
			type: type
		})
		if(type === 'price'){
			changePriceType(this.state.asc)
		}else{
			changeSearchType(type)
		}
	},
	render: (function(){
		var types = [{
			key: 'default',
			value: '默认'
		},{
			key: 'sale',
			value: '销量'
		},{
			key: 'latest',
			value: '最新'
		}]
		return function(){
			var {asc,type} = this.state
			var options = types.map(function(t,index){
				return (
					<a href=""
						className={type === t.key ? 'on' : ''}
						onClick={this.handler}
						data-type={t.key}
						key={index}
					>
						<span>{t.value}</span>
					</a>
				)
			}.bind(this))
			var priceClassName = cx({
				'on': type === 'price',
				'price-asc': asc,
				'price-desc': !asc
			})
			options.push(
				<a href=""
					className={priceClassName}
					key="price"
					onClick={this.handler}
					data-type="price"
				>
					<span>价格</span>
				</a>
			)
			return (
				<div className="searchOptions">
					{options}
				</div>
			)
		}
	})()
})

var searchResult = React.createClass({
	mixins: [Reflux.listenTo(searchStore,'getData'),Reflux.listenTo(Store,'getInitData'),toggleViewMixin],
	getInitialState() {
		var condition = this.queryStringify(this.props.query)
		return {
			list: true,
			condition: condition || '',
			page: 0,
			type: 'default'
		}
	},
	getInitData(data) {
		var self = this
		this.setState({
			initData: data,
			brands: util.getBrands(data)
		})
		searchActions.getData(this.state.condition + '&o=' + this.state.type)
	},
	componentWillMount: function() {
		require('../../css/searchResult.scss')
	},
	componentDidMount() {
		this.initialize({
			container: this.refs.search.getDOMNode(),
			wrapper: this.refs.wrapper.getDOMNode(),
			bindScrollEvent: true
		},function(){
			Actions.getData()
		})
		util.searchHistory.add(this.getSearchKey('k'))
	},
	componentWillUnmount() {
		this.unbind(this.refs.search.getDOMNode())
	},
	loadMoreCb(page) {
		return searchActions.getData(this.state.condition + '&o=' + this.state.type+'&p='+page)
	},
	freshCb() {
		return searchActions.getData(this.state.condition + '&o=' + this.state.type+'&p=0',true)
	},
	changeSearchType(type) {
		var self = this
		this.setState({
			type: type,
			loading: true,
			loadingCb: function(){
				searchActions.getData(self.state.condition + '&o=' + type,true)
			}
		})
	},
	changePriceType(asc) {
		var type,self = this
		if(asc) {
			type = 'price_a2z'
		}else{
			type = 'price_z2a'
		}
		this.setState({
			type: type,
			loading: true,
			loadingCb: function(){
				searchActions.getData(self.state.condition + '&o=' + type,true)
			}
		})
	},
	queryStringify(query) {
		if(!query) {
			return ''
		}
		var ret = []
		for(var i in query){
			if(query.hasOwnProperty(i)){
				ret.push(i + '=' + query[i])
			}
		}
		return ret.join('&')
	},
	getSearchKey(key) {
		var condition = this.props.query
		if(!condition) return ''
		return condition[key]
	},
	renderView() {
		var {data,brands,list,loading} = this.state
		if(data) {
			if(data.length){
				return <ToggleViewContent list={list} data={data} brands={brands}/>
			}else{
				return <div className="noresult"></div>
			}
		}
		return null
	},
	render() {
		var {data,list,loading,noTransition,loadingCb,sortData,wrapper} = this.state
		return (
			<div className="app-searchResult">
				<header>
					<div className="searchInput">
						<BackBtn to={this.props.prePath || 'main'} />
						<div className="searchTextWrapper">
							<SearchText value={this.getSearchKey('k')} useLink />
						</div>
						<ToggleViewToggler list={list} handler={this.toggle}/>
					</div>
					<SearchOptions
						changeSearchType={this.changeSearchType}
						changePriceType={this.changePriceType}
					/>
				</header>
				<section className="toggleView main" ref="search">
					<div className="wrap" ref="wrapper">
						<Refresher
							loading={loading}
							loadingCb={loadingCb}
							fresh={this.fresh}
							wrapper={wrapper}
							noTransition={noTransition}
						/>
						{this.renderView()}
					</div>
				</section>
			</div>
		)
	}
})

module.exports = searchResult
