var M = require('../components/ota.js/index.js'),
	config = require('../config')
var url = config.api.url + '/product/'
var Actions = Reflux.createActions(['getData'])
var Store = Reflux.createStore({
	listenables: [Actions],
	onGetData(id) {
		var self = this
		M.ajax({
			url: url + id,
			dataType: 'json',
			crossdomain: true,
			success(data) {
				// 构建便于筛选的数据结构
				self.trigger(self.buildSkus(data))
			},
			error(err) {
				self.trigger(err)
			}
		})
	},
	buildSkus(data) {
		// 每个分类下面保存存在该分类值的产品id
		var skus = data.skus,
			attrs = data.attrs
		if(Object.keys(attrs).length) {
			Object.keys(skus).forEach(function(key) {
				var sku = skus[key]
				Object.keys(sku.attr).forEach(function(attr) {
					var val = sku.attr[attr]
					attrs[attr][val] = attrs[attr][val] || []
					if (sku.qty && attrs[attr][val].indexOf(sku.stockId) === -1) {
						attrs[attr][val].push(sku.stockId)
					}
				})
			})
		}
		return data
	}
})

module.exports = {
	Actions, Store
}