var M = require('../components/ota.js/index.js'),
	config = require('../config'),
	util = require('../util')
var url = config.api.url + '/sys/init?v=1'
var Actions = Reflux.createActions(['getData','reset','fresh'])
var Store = Reflux.createStore({
	lock: false,
	listenables: [Actions],
	onGetData(fresh) {
		var self = this,
			data = util.mainStorage.get()
		if(data){
			// localstorage
			return this.trigger(data)
		}
		if(this.lock) return
		this.lock = true
		M.ajax({
			url: url,
			dataType: 'json',
			crossdomain: true,
			success(data){
				util.mainStorage.save(data)
				self.trigger(data,fresh)
				self.lock = false
			},
			error(err){
				self.trigger(err)
				self.lock = false
			}
		})
	},
	onReset() {
		this.lock = false
	},
	onFresh() {
		this.lock = false
		util.mainStorage.remove()
		Actions.getData(true)
	}
})

module.exports = {
	Actions,Store
}
