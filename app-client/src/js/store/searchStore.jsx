var M = require('../components/ota.js/index.js'),
	config = require('../config')
var url = config.api.url + '/product/search'
var Actions = Reflux.createActions(['getData'])
var Store = Reflux.createStore({
	lock: false,
	listenables: [Actions],
	onGetData(condition,fresh) {
		var self = this,query = condition ? ('?' + condition) : ''
		if(this.lock) return
		this.lock = true
		M.ajax({
			url: url + query,
			dataType: 'json',
			crossdomain: true,
			success(data){
				self.trigger(data,getMaxPage(data),fresh)
				self.lock = false
			},
			error(err){
				self.trigger(err)
				self.lock = false
			}
		})
	}
})

function getMaxPage(data){
	return Math.floor(data.totals / 20)
}

module.exports = {
	Actions,Store
}