var M = require('../components/ota.js/index.js'),
	config = require('../config')
var url = config.api.url + '/product/search'
var Actions = Reflux.createActions(['getData','fresh','reset'])
var Store = Reflux.createStore({
	data: [],
	lock: false,
	listenables: [Actions],
	onGetData(type,condition,fresh) {
		var self = this,query = condition ? ('?' + condition) : ''
		if(this.data[type] && this.data[type][condition]){
			return this.trigger(this.data[type][condition],getMaxPage(this.data[type][condition]))
		}
		if(this.lock) return
		this.lock = true
		M.ajax({
			url: url + query,
			dataType: 'json',
			crossdomain: true,
			success(data){
				self.data[type] = self.data[type] || {}
				self.data[type][condition] = data
				self.trigger(data,getMaxPage(data),fresh)
				self.lock = false
			},
			error(err){
				self.trigger(err)
				self.lock = false
			}
		})
	},
	onFresh(type,condition) {
		this.data[type] = undefined
		delete this.data[type]
		Actions.getData(type,condition,true)
	},
	onReset(type) {
		this.data[type] = undefined
		delete this.data[type]
	}
})

function getMaxPage(data){
	return Math.floor(data.totals / 20)
}

module.exports = {
	Actions,Store
}
