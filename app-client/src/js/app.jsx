var routes = require('./controller/index.jsx'),
	M = require('M')
window.SNT = require('./util.js').SNT
require('../css/base.scss')
require('babel/polyfill')

;(function(){
	var scale = function(){
		var w = M.get(window).width(),width
		if(w > 640){
			width = 640
		}else if(w < 320){
			width = 320
		}else{
			width = w
		}
		M.get('html').css({
			'font-size': 20  / 320 * width + 'px'
		})
	}
	// window.addEventListener('resize',scale)
	M.ready(scale)
})()

React.initializeTouchEvents(true) //开启触摸事件

var prePath = null
ReactRouter.run(routes,ReactRouter.HashLocation,function(Handler,state){
	React.render(<Handler prePath={prePath}/>,document.body)
	prePath = state.path
})
