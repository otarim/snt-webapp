var SNT = Object.defineProperties({}, {
	events: {
		value: {}
	},
	on: {
		value(eventName, cb) {
			this.events[eventName] = function(e) {
				e = e._args ? [e].concat(e._args) : [e]
				cb.apply(null, e)
			}
			window.addEventListener(eventName, this.events[eventName])
		}
	},
	off: {
		value(eventName) {
			var cb = this.events[eventName]
			if (!cb) return
			window.removeEventListener(eventName, cb)
			this.events[eventName] = cb = null
			delete this.events[eventName]
		}
	},
	fire: {
		value(eventName) {
			var evt = document.createEvent('CustomEvent'),
				args = [].slice.call(arguments, 1)
			evt.initEvent(eventName, true, true)
			args && (evt._args = args)
			window.dispatchEvent(evt)
		}
	}
})

var searchHistory = {
	key: 'snt.searchHistory',
	add(value) {
		var historys = JSON.parse(localStorage.getItem(this.key)) || []
		if (historys.indexOf(value) === -1) {
			historys.push(value)
		}
		localStorage.setItem(this.key, JSON.stringify(historys))
		return historys
	},
	getAll() {
		return JSON.parse(localStorage.getItem(this.key)) || []
	}
}

var mainStorage = {
	key: 'snt.mainStorage',
	save(data) {
		localStorage.setItem(this.key, JSON.stringify(data))
	},
	get() {
		return JSON.parse(localStorage.getItem(this.key))
	},
	remove() {
		localStorage.removeItem(this.key)
	}
}

var device = {
	userAgent: navigator.userAgent,
	isMobile: 'touchstart' in document,
	isiOS: navigator.userAgent.match('iPad') || navigator.userAgent.match('iPhone') || navigator.userAgent.match('iPod'),
	isAndroid: navigator.userAgent.match('Android'),
	isWechat: typeof WeixinJSBridge !== 'undefined',
	isWebView: navigator.userAgent.match('SNT-APP'),
	PixelRatio: window.devicePixelRatio
}

var clone = function(source) {
	var ret = {}
	Object.keys(source).forEach(function(key) {
		ret[key] = source[key]
	})
	return ret
}

var getFilter = function(source, condition) {
	var keys = Object.keys(condition),
		source = clone(source),
		passed = 0
	keys.forEach(function(key) {
		if (source[key] === condition[key]) {
			source[key] = null
			delete(source[key])
			passed += 1
		}
	})
	if (passed === keys.length) {
		return source
	}
}

var filterKey = function(sources, condition) {
	var ret = []
	sources.forEach(function(source) {
		var processedSource = getFilter(source, condition)
		if (processedSource) {
			ret.push(processedSource)
		}
	})
	return ret
}

var combineObj = function(arr) {
	var ret = {}
	arr.forEach(function(obj) {
		Object.keys(obj).forEach(function(key) {
			ret[key] = ret[key] || []
			if (ret[key].indexOf(obj[key]) === -1) {
				ret[key].push(obj[key])
			}
		})
	})
	return ret
}

var getBrands = function(data) {
	var ret = {}
	data.api.updates.brands.forEach(function(d) {
		if (d.logo) {
			ret[d.id] = d.logo
		}
	})
	return ret
}

var adpImage = function(img, size) {
	if (img.indexOf('!') !== -1) {
		size = parseInt(img.slice(img.indexOf('!') + 1), 10)
		img = img.replace('!' + size, '')
	}
	return img + '!' + device.PixelRatio * size
}

var lazy = function(html) {
	// 匹配src为#src
	return html.replace(/<img([^>]*)src="([^"]*)"([^>]*)>/g, function(str, $1, $2, $3) {
		return '<img' + $1 + 'src="dist/img/blank.png" data-lazy="' + $2 + '"' + $3 + '/>'
	})
}

module.exports = {
	searchHistory, mainStorage, device, SNT, filterKey, combineObj, getBrands, adpImage, lazy
}