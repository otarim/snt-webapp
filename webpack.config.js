var webpack = require('webpack'),
	CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin,
	UglifyJsPlugin = webpack.optimize.UglifyJsPlugin,
	path = require('path')

var ExtractTextPlugin = require("extract-text-webpack-plugin")
module.exports = {
	entry: {
		index: './app-client/src/js/app.jsx'
	},
	output: {
		path: './app-client/dist/js/',
		filename: '[name].min.js'
	},
	module: {
		loaders: [
			// {
			// 	test: /\.jpg$/,
			// 	loader: 'url-loader?limit=10000&minetype=image/jpg'
			// },
			// {
			// 	test: /\.png$/,
			// 	loader: 'url-loader?limit=10000&minetype=image/png'
			// },
			{
				test: /\.png$/,
				loader: 'file-loader?name=../img/[name].[ext]'
			},
			{
				test: /\.js$/,
				loader: 'babel',
				include: [path.join(__dirname,'app-client/src/js/')],
				exclude: [path.join(__dirname,'app-client/src/js/components')]
			},
			{
				test: /\.jsx$/,
				loader: 'jsx!babel'
			},{
				test: /\.scss$/,
				// loader: 'style!css!autoprefixer?{browsers:["Chrome > 1"]}!sass'
				loader: ExtractTextPlugin.extract("style-loader", 'css!autoprefixer?{browsers:["Chrome > 1"]}!sass'),
			}
		],
		// noParse: ['./app-client/src/js/components/react/react-with-addons.js']
	},
	resolve: {
		root: [path.join(__dirname,'app-client/src/js/')],
		extensions: ['','.js','.jsx'],
		alias: {
			'M': 'components/ota.js/index.js',
			'classSet': 'components/classnames/index.js',
			// 'React': 'components/react/react-with-addons.js'
		}
	},
	plugins: [
		// new UglifyJsPlugin({
		// 	mangle: {
		//         except: ['$super', '$', 'exports', 'require']
		//     }
		// })
        // new CommonsChunkPlugin("commons.js", ["pageA", "pageB", "admin-commons.js"], 2),
        // new CommonsChunkPlugin("c-commons.js", ["pageC", "adminPageC"]),
      new ExtractTextPlugin('../css/style.css',{chunkAll: true})
    ]
}
